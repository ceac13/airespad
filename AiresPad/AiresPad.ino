/* 
 * To convert the serial commands of this program to midi use the great Hairless MIDI (https://projectgus.github.io/hairless-midiserial/)
 * On windows you'll problably need use a program like the loopMIDI (https://www.tobias-erichsen.de/software/loopmidi.html) to generate 
 * a virtual midi port. On MacOS or Linux this will not be necessary.
 */

#define  midichannel 1

// States
#define DISABLED 1
#define WAITING  2 // Waiting the threshold.
#define BLOCKING 3 // Skipping the first values during [blockTime] milliseconds.
#define SCANNING 4 // Getting the maximum value during [scanTime] milliseconds.
#define MASKING  5 // Blocking others triggers for [maskTime] milliseconds.
#define DOWNING  6 // It indicate the moment after a threshold when the wave from the pad are still going down. The limit is [downingLimit].
#define HIHAT    7 // It indicate which pin is the hi-hat pedal controller.

int retriggerThreshold = 400; // When in DOWNING state only change to BLOCKING when the input value is equal or bigger than this.
int downingLimit = 20; // Lower than this value the state change from DOWING to WAITING.
long blockTime = 4; // How many milliseconds should the first values be ignored.
long scanTime = 7; // How many milliseconds waits for the maximum value before send the midi command.
long maskTime = 50; // How many milliseconds ignore a new threshold after a midi command is send.

//                         HH PEDAL   CRASH    BELL     RIDE     TOM 1    TOM 2   SPLASH     RIM     SNARE   HI-HAT    KICK    EXTRA    EXTRA
char pinAssignments[13] = {A0      , A1     , A2     , A3     , A4     , A5     , A6     , A7     , A8     , A9     ,  A10   ,  A11   ,  A12   };
byte padNote[13] =        { 4      ,   49   , 53     , 51     , 48     , 43     ,  1     , 37     , 38     , 18     ,  36    ,   5    ,    6   }; // MIDI notes from 0 to 127 (Mid C = 60)
int status[13] =          {HIHAT   , WAITING, WAITING, WAITING, WAITING, WAITING, WAITING, WAITING, WAITING, WAITING, WAITING, WAITING, WAITING}; 
long statusChanged[13] =  {0       , 0      , 0      , 0      , 0      , 0      , 0      , 0      , 0      , 0      , 0      , 0      , 0      };
int maxValues[13] =       {0       , 0      , 0      , 0      , 0      , 0      , 0      , 0      , 0      , 0      , 0      , 0      , 0      };
int threshold[13] =       {0       , 90     , 70     , 60     , 90     , 80     , 120    , 90     , 30     , 90     , 90     , 90     , 90     };
int numberOfPads = 13;
int hihatLastValue = 0;

void setup() {
  Serial.begin(57600);   
  for (int i = 0; i < numberOfPads; i++) {
    pinMode(pinAssignments[i], INPUT);
  }
}

void loop() {
  for (int i = 0; i < numberOfPads; i++) {
    switch(status[i]) {
      case WAITING:
        waiting(i);
        break;
      case BLOCKING:
        blocking(i);
        break;
      case SCANNING:
        scanning(i);
        break;
      case MASKING:
        masking(i);
        break;
      case DOWNING:
        downing(i);
        break;
      case HIHAT:
        hihat(i);
        break;
    }
  }
}

void waiting(int i) {
  int value = analogRead(pinAssignments[i]);
  if (value >= threshold[i]) {
    statusChanged[i] = millis();
    status[i] = BLOCKING;
  }
}

void blocking(int i) {
  if ((millis() - statusChanged[i]) > blockTime) {
    statusChanged[i] = millis();
    status[i] = SCANNING;
    maxValues[i] = 0;
  }
}

void scanning(int i) {
  if ((millis() - statusChanged[i]) <= scanTime) {
    maxValues[i] = max(maxValues[i], analogRead(pinAssignments[i]));
  } else {
    triggerIn(i);
    status[i] = MASKING;
    statusChanged[i] = millis();
  }
}

void masking(int i) {
  if ((millis() - statusChanged[i]) > maskTime) {
    triggerOut(i);
    status[i] = DOWNING;
  }
}

void downing(int i) {
  int value = analogRead(pinAssignments[i]);
  if (value >= retriggerThreshold) {
    statusChanged[i] = millis();
    status[i] = BLOCKING;
  } else if (value < downingLimit) {
    status[i] = WAITING;
  }
}

void triggerIn(int i) {
  // Send midi start
  int velocity = calculateVelocity(maxValues[i], i);
  sendMidi(144,padNote[i],velocity);
}

void triggerOut(int i) {
  // Send mide end
  sendMidi(144,padNote[i],0);
}

void sendMidi(byte MESSAGE, byte PITCH, byte VELOCITY) {
  byte status1 = MESSAGE + midichannel;
  Serial.write(status1);
  Serial.write(PITCH);
  Serial.write(VELOCITY);
}

int calculateVelocity(int value, int pin) {
  int minimo = 20;
  int velocity = log(value) * 18.3;
  if (velocity < minimo)
    return minimo;
  else if (velocity > 127)
    return 127;
  else
    return velocity;  
}

void hihat(int pin) {
  long maxx = 430;
  long minn = 50;
  int margin = 8;
  
  int value = analogRead(pin);
  
  long velocit = ((value - minn))*127 / ((maxx-minn));
  if (velocit > 118) velocit = 127;
  if (velocit < 0) velocit = 0;
  
  long previousVelocit = (hihatLastValue - minn)*127 / (maxx-minn);
  if (previousVelocit > 127) previousVelocit = 127;
  if (previousVelocit < 0) previousVelocit = 0;

  if (fabs(previousVelocit - velocit) > margin) {
    hihatLastValue = value;
  
    sendMidi(176, padNote[pin], (int) velocit);
  }
}
