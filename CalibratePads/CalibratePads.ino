/*
This program is used to see how much signal each pad is producing. 
In my e-drums I hitted the pad as hard is possible and adjusted the trimpot until get the maximum value around 1000.
Use the Monitor Serial to see the values.
*/

#define PIN A5 // Set the pin you'll gonna test

int last = 0;
int maxx = 0;

void setup() {
  Serial.begin(57600); 
  pinMode(PIN, INPUT);
}

void loop() {  
  //showCurrentValue();
  showMaxValue();
}

void showMaxValue() {
  int value = analogRead(PIN);
  
  if (value > maxx) {
    maxx = value;
    Serial.println(maxx);
  }
}

void showCurrentValue() {
  int value = analogRead(PIN);
  if (value != last && value > 0) {
    last = value;
    Serial.println(last);
  }
}
